# Chef-FSM

Chef-fsm is a platform-agnostic C++ library that provides classes and tools to
implement your Finite State Machines. It is header only, and has no dependencies
beside the C++ standard library.

## The idea

The library provides a state machine engine that powers transitions between 
states, and calling the proper onEnter, onExit and onEvent methods. The library
is not intrusive, and does not require to derive from any base class. It is also
not intrusive in the sense that it does not require to use any specific type for
events.

States are modeled with member functions of your implementation class.

Your class provides the state implementation, and the state machine engine calls
the proper methods when needed.

Events are fed to the state machine engine, and the engine dispatch it to the 
current state.


## Example

In the following example, we define a simple state machine that models a traffic
light. The traffic light has three states: Red, Green and Yellow. The traffic
light has two events: ButtonPressed and Timeout.

The ButtonPressed is for pedestrians to ask for a green light. The Timeout 
event is for the traffic light to change state after a certain time. The button
acts only when the light is green (for the cars), causing the light to change to
yellow.

```cpp
#include <ChefFsm/StateMachine.hh>

/* note 1 */
struct ButtonPressed {};
struct Timeout {};

/* note 2 */
template<class... Ts>
struct overloaded : Ts... { using Ts::operator()...; };
// explicit deduction guide (not needed as of C++20)
template<class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;


/* host class */
class TrafficLight;

/* note 3 */
using Fsm = ChefFsm::StateMachine<TrafficLight,ButtonPress,Timeout>;

class TrafficLight {
public:
    
    TrafficLight()
    : fsm(this, &TrafficLight::stateRed) /* note 4 */
    {
    }

    /* note 5 */
    template<typename E>
    void feed( E event )
    {
        fsm.feed( event );
    }
    
    Fsm::State stateRed( Fsm::StateArg const& event )
    {
        return std::visit(
            overloaded{
                [this](Fsm::OnEntry) { /* note 6 */
                    setTimer( RedTimeout );
                    return Fsm::Stay;   /* note 7 */
                },
                [this](Timeout) {
                    return &TrafficLight::stateGreen; /* note 8 */
                },
                [this](auto _) { /* note 9 */
                    return Fsm::Stay;
                }
            },
            event   /* note 10 */
        );
    }
    
    Fsm::State stateGreen( Fsm::StateArg const& event )
    {
        return std::visit(
            overloaded{
                [this](Fsm::OnEntry) {
                    setTimer( GreenTimeout );
                    return Fsm::Stay;
                },
                [this](ButtonPressed) {
                    clearTimer();
                    return &TrafficLight::stateYellow;
                },
                [this](Timeout) {
                    return &TrafficLight::stateYellow;
                },
                [this](Fsm::OnExit) {
                    return Fsm::Stay;
                }
            },
            event
        );
    }
    
    Fsm::State stateYellow( Fsm::StateArg const& event )
    {
        return std::visit(
            overloaded{
                [this](Fsm::OnEntry) {
                    setTimer( YellowTimeout );
                    return Fsm::Stay;
                },
                [this](Timeout) {
                    return &TrafficLight::stateRed;
                },
                [this](auto _) {
                    mTrace.push_back(Trace::YellowExit);
                    return Fsm::Stay;
                }
            },
            event
        );
    }
    
private:
    Fsm fsm;
};
```

Notes:

1. We define the events that the state machine will handle. These are simple 
   structs, but they could be anything that can be used as a variant type. 
   Possibly your events will be richer than this, and will carry data.
2. We define a helper template to use with std::visit. This allows a basic, yet
   effective way to dispatch events to the proper function.
3. A nickname for the state machine type is convenient to avoid repeating the 
   long type name.
4. In the constructor we initialize the state machine with the host object and 
   the initial state.
5. The feed method is just a forwarder to the inner state machine. This has been
   made public, but it could be private if events are generated inside the host
   class. Also can be turned into specific methods for each event type.
6. Fsm::OnEntry and Fsm::OnExit are just markers for the state machine to call 
   the state enter and exit handlers. They are not events, but they are used as
   such in the state implementation.
7. Returning Fsm::Stay means that the state machine should not change state. 
   More specifically return values from OnEnter and OnExit are ignored.
8. Returning a pointer to a state member function means that the state machine 
   should change state to the returned one. When something different from 
   `Fsm::Stay` is returned to the state machine engine, the onExit for the
   current state is called and then the onEntry for the new state is called.
   Note that if you return a pointer to the same state member function, the
   state stays the same, but the onExit and onEntry are called. This can be
   sometimes convenient.
9. The `auto _` is a catch-all for events that are not handled. This helps in
   saving some typing, especially when you have many possible events that needs
   to be ignored. But if you prefer, you may be explicit listing all the events.
10. Don't forget to add the event parameter to the std::visit call. This is the
    event that is being dispatched to the proper state member function.
