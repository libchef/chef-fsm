# Chef-FSM

## Description

Chef-fsm is a platform-agnostic C++ library that provides classes and tools to
implement your Finite State Machines. It is header only, and has no dependencies
beside the C++ standard library.

The goal is to provide the C++ programmer with a reusable and solid way to 
implement FSMs. The library is not intrusive, and does not require to derive
from any base class. It is also not intrusive in the sense that it does not
require to use any specific type for events.

States are modeled with member functions of your implementation class. 

## Getting started

The library is header only, so you may download the files and place them in
your include path.

If you are using cmake, you may use the following code to make ChefFun available
to your project:

```cmake
cmake_minimum_required(VERSION 3.14)

#...

include( FetchContent )

FetchContent_Declare(
    chef-fsm
    GIT_REPOSITORY https://gitlab.com/libchef/chef-fsm.git
    GIT_TAG        1.0.0
)

FetchContent_MakeAvailable( chef-fsm )

target_include_directories(
    YourTarget
    PUBLIC
        ${chef-fsm_SOURCE_DIR}/include
        # other include paths
)
```

## Support
The code is provided as is, there is no granted support.
You may contact me via email, but I cannot guarantee any answer.

## Roadmap
Debugging tools, calls to substate machine and DSL are planned.

## Contributing
Users are welcome, contributions even more. Forking and proposing MR are the
preferred way to make contributions. Feel free to contact the author.

Authors and acknowledgment
see [AUTHORS] file.

License
Apache License v2.0

## Project status

The project is based on my professional experience and the code is close to code 
used in my industrial projects. 