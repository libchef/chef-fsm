/**
 * @date 2024-02-14
 * @author Massimiliano Pagani
 * @file ChefFsm/StateMachine.hh
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#if !defined( CHEFFSM_STATE_MACHINE_DSL_HH )
#define CHEFFSM_STATE_MACHINE_DSL_HH

namespace ChefFsm {
    template<class... Ts>
    struct overloaded : Ts... { using Ts::operator()...; };
    /* explicit deduction guide (not needed as of C++20) */
    template<class... Ts>
    overloaded(Ts...) -> overloaded<Ts...>;

}

#define CHEFFSM_declareFsmType( H, Ev... ) \
    using FsmType = ChefFsm::StateMachine<H, Ev>;

#define CHEFFSM_declareState( S ) \
    FsmType::State state##S( FsmType::StateArg const& event )

#define CHEFFSM_StateBegin( H, N )                                         \
    H::FsmType::State H::state##N( H::FsmType::StateArg const& chefFsmStateMachine_event ) { \
        using ChefFsmHostType = H;                                                                   \
        return std::visit(                                                  \
            ChefFsm::overloaded{

#define CHEFFSM_StateEnd                                                    \
                    return FsmType::Stay;                                   \
                }                                                           \
            },                                                              \
            chefFsmStateMachine_event                                       \
        );                                                                  \
    }

#define CHEFFSM_OnEntry                                                     \
                [this](FsmType::OnEntry) {

#define CHEFFSM_OnExit                                                      \
                    return FsmType::Stay;                                   \
                },                                                          \
                [this](auto const&) {                                       \
                    return FsmType::Stay;                                   \
                },                                                          \
                [this](FsmType::OnExit) {

#define CHEFFSM_OnEvent( E )                                                \
                    return FsmType::Stay;                                   \
                },                                                          \
                [this](E) {

#define CHEFFSM_OnDefault()                                                 \
                    return FsmType::Stay;                                   \
                },                                                          \
                [this](auto const&) {

#define CHEFFSM_TransitionTo( S )                                           \
                return &ChefFsmHostType::state##S

#define CHEFFSM_StateEndWithDefault( SM, N )                                \

#endif // CHEFFSM_STATE_MACHINE_DSL_HH