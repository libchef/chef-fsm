/**
 * @date 2024-01-05
 * @author Massimiliano Pagani
 * @file ChefFsm/StateMachine.hh
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if !defined( CHEFFSM_STATE_MACHINE_HH )
#define CHEFFSM_STATE_MACHINE_HH

#include <variant>

namespace ChefFsm
{
    /**
     * @brief A simple state machine implementation.
     *
     * This template implements a state machine engine that can be used to
     * power a wide range of state machines.
     *
     * The state machine is defined by a host class and a set of event types.
     *
     * The host class is the class that will contain the specific part of the
     * state machine and defines the specific functionality.
     *
     * You can use one or several different types to model events, freeing the
     * state machine from the need to define a universal event type.
     *
     * A state in the host class is defined by a function that accepts a variant
     * of all the possible events and returns the next state to transition to.
     *
     * The state handling can be implemented by using a std::visit on the
     * variant and a set of lambdas that handle the different events.
     *
     * @tparam T The host class of the state machine.
     * @tparam Es The event types that the state machine can handle.
     */
    template<typename T,typename ...Es>
    class StateMachine
    {
    public:
        /**
         * State event tags.
         * The state will receive an event tagged as OnEntry when the state
         * machine executes a transition to that state.
         * The state will receive an event tagged as OnExit when the state
         * machine executes a transition from that state.
         * @{
         */
        struct OnEntry {};
        struct OnExit {};
        /** @} */

        /**
         * Shortcut for the state function argument type. Using this type
         * avoids to repeat each time the full variant with all the event
         * types.
         */
        using StateArg = std::variant<OnEntry,OnExit,Es...>;

        /**
         * The State reference type.
         *
         * This class is used to define the state of the state machine, it
         * includes a pointer to the host member function that implements the
         * state and a method to feed the state with an event.
         */
        class State
        {
        public:
            /**
             * The state function pointer type.
             */
            using StateFn = State (T::*)(StateArg const&);

            /**
             * The default constructor.
             *
             * @param stateFn the member function pointer of the state, Being
             *                implicit, there is no need to call it explicitly
             *                when returning the next state from the state
             *                function.
             * @note this constructor is intended to be implicit
             */
            State( StateFn stateFn ) noexcept;   // NOLINT

            /**
             * Feed the state with an event.
             *
             * @param host the host object of the state machine.
             * @param arg the event to feed to the state.
             * @return the next state to transition to. If this is null, then
             *         there is no transition.
             */
            State feed( T& host, StateArg arg );

            /**
             * Check if the state is null.
             *
             * @retval true if this object has been constructed with a nullptr
             * @retval false if this object has been constructed with a valid
             *               member function pointer
             */
            [[nodiscard]] bool isNull() const noexcept;

            /**
             * Comparison operators
             *
             * @param other the object to compare with
             * @return the comparison result
             * @{
             */
            [[nodiscard]] bool operator==( State const& other ) const;
            [[nodiscard]] bool operator!=( State const& other ) const;
            /** @} */
        private:
            StateFn mStateFn;
        }; // as defined previously

        /**
         * Convenience value for returning a nullptr to request no transition.
         */
        static auto constexpr Stay = static_cast<State::StateFn>(nullptr);

        /**
         * The class constructor.
         *
         * @param host the host object where state functions are.
         * @param initialState the initial state of the state machine.
         */
        explicit StateMachine(T* host, State::StateFn initialState );

        /**
         * Feed the state machine with an event.
         *
         * @param event the event to feed to the state machine.
         */
        template<typename E>
        void feed( E&& event );

    private:
        void feedImpl( StateArg const &arg );

        State mState;
        T* mHost;
    };

    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // ::::::::::::::::::::::::::::::::::::::::::::: method implementations :::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    template<typename T,typename ...Es>
    StateMachine<T,Es...>::State::State( StateFn stateFn ) noexcept
    : mStateFn(stateFn )
    {}

    template<typename T,typename ...Es>
    auto StateMachine<T,Es...>::State::feed( T& host, StateArg arg ) -> State
    {
        return (host.*mStateFn)(arg );
    }

    template<typename T,typename ...Es>
    inline bool StateMachine<T,Es...>::State::isNull() const noexcept
    {
        return mStateFn == nullptr;
    }

    template<typename T,typename ...Es>
    inline bool
    StateMachine<T,Es...>::State::operator==( State const& other ) const
    {
        return mStateFn == other.mStateFn;
    }

    template<typename T,typename ...Es>
    inline bool
    StateMachine<T,Es...>::State::operator!=( State const& other ) const
    {
        return mStateFn != other.mStateFn;
    }

    template<typename T,typename ...Es>
    StateMachine<T,Es...>::StateMachine(T* host, State::StateFn initialState )
    : mState{ initialState }
    , mHost{ host }
    {
        mState.feed( *host, StateArg(OnEntry{}) );
    }

    template<typename T,typename ...Es>
    template<typename E>
    void StateMachine<T,Es...>::feed( E&& event )
    {
        StateArg arg = std::forward<E>(event);
        feedImpl( arg );
    }

    template<typename T,typename ...Es>
    void StateMachine<T,Es...>::feedImpl( StateArg const &arg )
    {
        auto nextState = mState.feed( *mHost, arg );
        if( !nextState.isNull() ) {
            mState.feed( *mHost, StateArg(OnExit{}) );
            mState = nextState;
            mState.feed( *mHost, StateArg(OnEntry{}) );
        }
    }
}

#endif  // CHEFFSM_STATE_MACHINE_HH