/**
 * @author Massimiliano Pagani
 * @date 2024-02+24.
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ChefFsm/StateMachine.hh>
#include <gtest/gtest.h>


/* Define your events */
struct ButtonPressed {};
struct Timeout {};

namespace {
    constexpr int RedTimeout = 10;
    constexpr int GreenTimeout = 10;
    constexpr int YellowTimeout = 10;
}

template<class... Ts>
struct overloaded : Ts... { using Ts::operator()...; };
// explicit deduction guide (not needed as of C++20)
template<class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

void setTimer( int timeout )
{
    (void)timeout;
    // do nothing, it's just a test
}
void clearTimer()
{
    // do nothing, it's just a test
}

/* host class */
class TrafficLight;

using Fsm = ChefFsm::StateMachine<TrafficLight,ButtonPressed,Timeout>;

enum class Trace
{
    RedEntry,
    RedExit,
    RedTimeout,
    RedButton,
    GreenEntry,
    GreenExit,
    GreenTimeout,
    GreenButton,
    YellowEntry,
    YellowExit,
    YellowTimeout,
    YellowButton
};

class TrafficLight {
public:
    std::vector<Trace> mTrace;
    
    TrafficLight()
    : fsm(this, &TrafficLight::stateRed)
    {
    }

    template<typename E>
    void feed( E event )
    {
        fsm.feed( event );
    }
    
    Fsm::State stateRed( Fsm::StateArg const& event )
    {
        return std::visit(
            overloaded{
                [this](Fsm::OnEntry) {
                    mTrace.push_back(Trace::RedEntry);
                    setTimer( RedTimeout );
                    return Fsm::Stay;
                },
                [this](ButtonPressed) {
                    // Red for cars, it's green for pedestrians
                    mTrace.push_back(Trace::RedButton);
                    return Fsm::Stay;
                },
                [this](Timeout) {
                    mTrace.push_back(Trace::RedTimeout);
                    return &TrafficLight::stateGreen;
                },
                [this](Fsm::OnExit) {
                    mTrace.push_back(Trace::RedExit);
                    return Fsm::Stay;
                }
            },
            event
        );
    }
    
    Fsm::State stateGreen( Fsm::StateArg const& event )
    {
        return std::visit(
            overloaded{
                [this](Fsm::OnEntry) {
                    mTrace.push_back(Trace::GreenEntry);
                    setTimer( GreenTimeout );
                    return Fsm::Stay;
                },
                [this](ButtonPressed) {
                    mTrace.push_back(Trace::GreenButton);
                    clearTimer();
                    return &TrafficLight::stateYellow;
                },
                [this](Timeout) {
                    mTrace.push_back(Trace::GreenTimeout);
                    return &TrafficLight::stateYellow;
                },
                [this](Fsm::OnExit) {
                    mTrace.push_back(Trace::GreenExit);
                    return Fsm::Stay;
                }
            },
            event
        );
    }
    
    Fsm::State stateYellow( Fsm::StateArg const& event )
    {
        return std::visit(
            overloaded{
                [this](Fsm::OnEntry) {
                    mTrace.push_back(Trace::YellowEntry);
                    setTimer( YellowTimeout );
                    return Fsm::Stay;
                },
                [this](ButtonPressed) {
                    mTrace.push_back(Trace::YellowButton);
                    // we are already transitioning to red (green for pedestrians)
                    return Fsm::Stay;
                },
                [this](Timeout) {
                    mTrace.push_back(Trace::YellowTimeout);
                    return &TrafficLight::stateRed;
                },
                [this](Fsm::OnExit) {
                    mTrace.push_back(Trace::YellowExit);
                    return Fsm::Stay;
                }
            },
            event
        );
    }
    
private:
    Fsm fsm;
};

TEST(StateMachineTrafficLight, RedToGreen)
{
    TrafficLight tl;
    tl.feed( ButtonPressed{} );
    ASSERT_EQ( tl.mTrace, (std::vector{
        Trace::RedEntry,
        Trace::RedButton,
    }));
    tl.mTrace.clear();
    tl.feed( Timeout{} );
    tl.feed( ButtonPressed{} );
    ASSERT_EQ( tl.mTrace, (std::vector{
        Trace::RedTimeout,
        Trace::RedExit,
        Trace::GreenEntry,
        Trace::GreenButton,
        Trace::GreenExit,
        Trace::YellowEntry
    }));
    tl.mTrace.clear();
    tl.feed( ButtonPressed{} );
    ASSERT_EQ( tl.mTrace, (std::vector{
        Trace::YellowButton,
    }));
    tl.mTrace.clear();
    tl.feed( Timeout{} );
    tl.feed( Timeout{} );
    tl.feed( Timeout{} );
    ASSERT_EQ( tl.mTrace, (std::vector{
        Trace::YellowTimeout,
        Trace::YellowExit,
        Trace::RedEntry,
        Trace::RedTimeout,
        Trace::RedExit,
        Trace::GreenEntry,
        Trace::GreenTimeout,
        Trace::GreenExit,
        Trace::YellowEntry
    }));

}