/**
 * @author Massimiliano Pagani
 * @date 2024-02-08
 * @file test/StateMachineTest.cc
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ChefFsm/StateMachine.hh>
#include <gtest/gtest.h>
#include <vector>


template<class... Ts>
struct overloaded : Ts... { using Ts::operator()...; };
// explicit deduction guide (not needed as of C++20)
template<class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

struct StringEvent { std::string message; };
struct IntEvent { int value; };
struct EmptyEvent {};

class FsmHost;

using Fsm = ChefFsm::StateMachine<FsmHost,
    StringEvent,
    IntEvent,
    EmptyEvent
>;

enum class Action
{
    EntryStateA,
    ExitStateA,
    EventStateA_String,
    EventStateA_Int,
    EventStateA_Empty,
    EntryStateB,
    ExitStateB,
    EventStateB_String,
    EventStateB_Int,
    EventStateB_Empty
};

class FsmHost
{
public:
    std::vector<Action> actions;

    std::string message;
    int value = 0;
    bool fail = false;

    FsmHost()
    : mFsm(this, &FsmHost::stateA)
    {
    }

    template<typename T>
    void feed( T&& button )
    {
        mFsm.feed( std::forward<T>(button) );
    }
private:
    Fsm::State stateA( Fsm::StateArg const& event ) {
        return std::visit(
            overloaded{
                [this](Fsm::OnEntry) {
                    actions.push_back(Action::EntryStateA);
                    return Fsm::Stay;
                },
                [this](Fsm::OnExit) {
                    actions.push_back(Action::ExitStateA);
                    return Fsm::Stay;
                },
                [this](StringEvent const& e) {
                    actions.push_back(Action::EventStateA_String);
                    message = e.message;
                    return message == "switchB" ?
                       &FsmHost::stateB :
                       Fsm::Stay;
                },
                [this](IntEvent e) {
                    actions.push_back(Action::EventStateA_Int);
                    value = e.value;
                    return value == 42 ?
                       &FsmHost::stateA :
                       Fsm::Stay;
                },
                [this](EmptyEvent) {
                    actions.push_back(Action::EventStateA_Empty);
                    return Fsm::Stay;
                },
                [&](auto _) {
                    fail = true;
                    return Fsm::Stay;
                }
            },
            event
        );
    }
    Fsm::State stateB( Fsm::StateArg const& event )
    {
        return std::visit(
            overloaded{
                [this](Fsm::OnEntry) {
                    actions.push_back(Action::EntryStateB);
                    return Fsm::Stay;
                },
                [this](Fsm::OnExit) {
                    actions.push_back(Action::ExitStateB);
                    return Fsm::Stay;
                },
                [this](StringEvent const& e) {
                    actions.push_back(Action::EventStateB_String);
                    message = e.message;
                    return message == "switchA" ?
                       &FsmHost::stateA :
                       Fsm::Stay;
                },
                [this](IntEvent e) {
                    actions.push_back(Action::EventStateB_Int);
                    value = e.value;
                    return value == 42 ?
                       &FsmHost::stateA :
                       Fsm::Stay;
                },
                [this](EmptyEvent) {
                    actions.push_back(Action::EventStateB_Empty);
                    return Fsm::Stay;
                },
                [&]( auto _ ) {
                    fail = true;
                    return Fsm::Stay;
                }
            },
            event
        );
    }

    Fsm mFsm;
};

TEST( FsmTest, test_no_transitions )
{
    FsmHost fsm;
    ASSERT_EQ( fsm.actions.size(), 1 );
    ASSERT_EQ( fsm.actions[0], Action::EntryStateA );
    fsm.actions.clear();

    fsm.feed( StringEvent{"Hello"} );
    ASSERT_EQ( fsm.actions, std::vector{Action::EventStateA_String} );
    ASSERT_EQ( fsm.message, "Hello" );
    fsm.actions.clear();

    fsm.feed( IntEvent{37} );
    ASSERT_EQ( fsm.actions.size(), 1 );
    ASSERT_EQ( fsm.actions[0], Action::EventStateA_Int );
    ASSERT_EQ( fsm.message, "Hello" );
    ASSERT_EQ( fsm.value, 37 );
    fsm.actions.clear();

    fsm.feed( EmptyEvent{} );
    ASSERT_EQ( fsm.actions.size(), 1 );
    ASSERT_EQ( fsm.actions[0], Action::EventStateA_Empty );
    ASSERT_EQ( fsm.message, "Hello" );
    ASSERT_EQ( fsm.value, 37 );
}

TEST( FsmTest, test_auto_transitions )
{
    FsmHost fsm;
    fsm.actions.clear();

    fsm.feed( IntEvent{42} );
    ASSERT_EQ( fsm.actions, (std::vector{
        Action::EventStateA_Int,
        Action::ExitStateA,
        Action::EntryStateA
    }));
}

TEST( FsmTest, test_transitions )
{
    FsmHost fsm;
    fsm.actions.clear();

    fsm.feed( StringEvent{"switchB"} );
    ASSERT_EQ( fsm.message, "switchB" );
    fsm.feed( StringEvent{"switchA"} );
    ASSERT_EQ( fsm.message, "switchA" );

    ASSERT_EQ( fsm.actions, (std::vector{
        Action::EventStateA_String,
        Action::ExitStateA,
        Action::EntryStateB,
        Action::EventStateB_String,
        Action::ExitStateB,
        Action::EntryStateA
    }));
}
