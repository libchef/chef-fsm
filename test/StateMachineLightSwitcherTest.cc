/**
 * @author Massimiliano Pagani
 * @date 2024-01-05
 * @file test/StateMachineLightSwitcherTest.cc
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ChefFsm/StateMachine.hh>
#include <gtest/gtest.h>


void gpioWrite( int );

template<class... Ts>
struct overloaded : Ts... { using Ts::operator()...; };
// explicit deduction guide (not needed as of C++20)
template<class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

struct ButtonPress
{
    int buttonCode;
};

class LightSwitcher;

using Fsm = ChefFsm::StateMachine<LightSwitcher,ButtonPress>;

class LightSwitcher
{
    public:
        LightSwitcher()
        : mFsm(this, &LightSwitcher::stateOff)
        {
        }

        void feed( ButtonPress button )
        {
            mFsm.feed( button );
        }
    private:
        Fsm::State stateOff( Fsm::StateArg const& event )
        {
            return std::visit(
                overloaded{
                    [](Fsm::OnEntry) {
                        gpioWrite(0);
                        return Fsm::Stay;
                    },
                    [](Fsm::OnExit) {
                        return Fsm::Stay;
                    },
                    [](ButtonPress e) {
                        return e.buttonCode == 42 ?
                           &LightSwitcher::stateOn :
                           Fsm::Stay;
                    },
                    []( auto _ ) {
                        return Fsm::Stay;
                    }
                },
                event
            );
        }

        Fsm::State stateOn( Fsm::StateArg const& event )
        {
            return  std::visit(
                overloaded{
                    [](Fsm::OnEntry) {
                        gpioWrite(1);
                        return Fsm::Stay;
                    },
                    [](Fsm::OnExit) {
                        return Fsm::Stay;
                    },
                    [](ButtonPress e) {
                        return e.buttonCode == 42 ?
                           &LightSwitcher::stateOff :
                           Fsm::Stay;
                    }
                },
                event
            );
        }

        Fsm mFsm;
};

int g_lastGpioWrite = -1;

void gpioWrite( int io )
{
    g_lastGpioWrite = io;
}

TEST( FsmTest, test )
{
    ASSERT_NE( g_lastGpioWrite, 0 );
    ASSERT_NE( g_lastGpioWrite, 1 );
    LightSwitcher cut;
    ASSERT_EQ( g_lastGpioWrite, 0 );
    cut.feed( ButtonPress{37} );
    ASSERT_EQ( g_lastGpioWrite, 0 );
    cut.feed( ButtonPress{42} );
    ASSERT_EQ( g_lastGpioWrite, 1 );
    cut.feed( ButtonPress{37} );
    ASSERT_EQ( g_lastGpioWrite, 1 );
    cut.feed( ButtonPress{42} );
    ASSERT_EQ( g_lastGpioWrite, 0 );
}
