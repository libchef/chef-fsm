/**
 * @author Massimiliano Pagani
 * @date 2024-02-14
 * @file test/StateMachineLightSwitcherTest.cc
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ChefFsm/StateMachine.hh>
#include <ChefFsm/StateMachineDsl.hh>
#include <gtest/gtest.h>

void gpioWrite( int );

class LightSwitcher;

enum class Trace
{
    RedEntry,
    RedExit,
    RedTimeout,
    RedButton,
    GreenEntry,
    GreenExit,
    GreenTimeout,
    GreenButton,
    YellowEntry,
    YellowExit,
    YellowTimeout,
    YellowButton
};

struct ButtonPressed {};
struct Timeout {};

namespace {
    constexpr int RedTimeout = 10;
    constexpr int GreenTimeout = 10;
    constexpr int YellowTimeout = 10;

    void setTimer( int timeout )
    {
        (void)timeout;
        // do nothing, it's just a test
    }
    void clearTimer()
    {
        // do nothing, it's just a test
    }

}


class TrafficLight
{
public:
    std::vector<Trace> mTrace;

    TrafficLight()
            : fsm(this, &TrafficLight::stateRed)
    {
    }

    template<typename E>
    void feed( E event )
    {
        fsm.feed( event );
    }

private:
    CHEFFSM_declareFsmType( TrafficLight, ButtonPressed, Timeout );

    CHEFFSM_declareState( Red );
    CHEFFSM_declareState( Green );
    CHEFFSM_declareState( Yellow );

    FsmType fsm;
};

CHEFFSM_StateBegin( TrafficLight, Red )
    CHEFFSM_OnEntry
        mTrace.push_back(Trace::RedEntry);
        setTimer( RedTimeout );
    CHEFFSM_OnExit
        mTrace.push_back(Trace::RedExit);
    CHEFFSM_OnEvent( ButtonPressed )
        // Red for cars, it's green for pedestrians
        mTrace.push_back(Trace::RedButton);
    CHEFFSM_OnEvent( Timeout )
        mTrace.push_back(Trace::RedTimeout);
        CHEFFSM_TransitionTo( Green );
    CHEFFSM_OnDefault()
        CHEFFSM_TransitionTo( Green );
    CHEFFSM_OnDefault()
        CHEFFSM_TransitionTo( Red );
CHEFFSM_StateEnd


CHEFFSM_StateBegin( TrafficLight, Green )
    CHEFFSM_OnEntry
        mTrace.push_back(Trace::GreenEntry);
        setTimer( GreenTimeout );
    CHEFFSM_OnEvent( ButtonPressed )
        mTrace.push_back(Trace::GreenButton);
        clearTimer();
        CHEFFSM_TransitionTo( Yellow );
    CHEFFSM_OnEvent( Timeout )
        mTrace.push_back(Trace::GreenTimeout);
        CHEFFSM_TransitionTo( Yellow );
    CHEFFSM_OnExit
        mTrace.push_back(Trace::GreenExit);
CHEFFSM_StateEnd

CHEFFSM_StateBegin( TrafficLight, Yellow )
    CHEFFSM_OnEntry
        mTrace.push_back(Trace::YellowEntry);
        setTimer( YellowTimeout );
    CHEFFSM_OnEvent( ButtonPressed )
        mTrace.push_back(Trace::YellowButton);
        // we are already transitioning to red (green for pedestrians)
    CHEFFSM_OnEvent( Timeout )
        mTrace.push_back(Trace::YellowTimeout);
        CHEFFSM_TransitionTo( Red );
    CHEFFSM_OnExit
        mTrace.push_back(Trace::YellowExit);
CHEFFSM_StateEnd


TEST(StateMachineDsl, RedToGreen)
{
    TrafficLight tl;
    tl.feed( ButtonPressed{} );
    ASSERT_EQ( tl.mTrace, (std::vector{
        Trace::RedEntry,
        Trace::RedButton,
    }));
    tl.mTrace.clear();
    tl.feed( Timeout{} );
    tl.feed( ButtonPressed{} );
    ASSERT_EQ( tl.mTrace, (std::vector{
        Trace::RedTimeout,
        Trace::RedExit,
        Trace::GreenEntry,
        Trace::GreenButton,
        Trace::GreenExit,
        Trace::YellowEntry
    }));
    tl.mTrace.clear();
    tl.feed( ButtonPressed{} );
    ASSERT_EQ( tl.mTrace, (std::vector{
        Trace::YellowButton,
    }));
    tl.mTrace.clear();
    tl.feed( Timeout{} );
    tl.feed( Timeout{} );
    tl.feed( Timeout{} );
    ASSERT_EQ( tl.mTrace, (std::vector{
        Trace::YellowTimeout,
        Trace::YellowExit,
        Trace::RedEntry,
        Trace::RedTimeout,
        Trace::RedExit,
        Trace::GreenEntry,
        Trace::GreenTimeout,
        Trace::GreenExit,
        Trace::YellowEntry
    }));

}